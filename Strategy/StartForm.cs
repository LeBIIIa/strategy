﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy
{
    public partial class StartForm : Form
    {
        private int n, m, k, mod;
        public StartForm()
        {
            InitializeComponent();
        }

        private void byMouse_Click(object sender, EventArgs e)
        {
            moves st = new moves();
            if (st.ShowDialog() == DialogResult.OK)
            {
                n = st.Row;
                m = st.Column;
                mod = st.Mode;
                k = st.CountMoves * 2;
            
                Base root = new Base(this, n, m, k, mod);
                root.Show();
                this.Hide();
            }
        }

        private void byFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDoc‌​uments);
            openFileDialog1.Filter = "strategy files (*.strategy)|*.strategy";
            openFileDialog1.Title = "Завантажити файл з грою";
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                bool fYes = false;
                FileStream fs = null;
                StreamReader sr = null;
                try
                {
                    Console.WriteLine(openFileDialog1.FileName);
                    if (File.Exists(openFileDialog1.FileName))
                    {
                        fs = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read);
                        sr = new StreamReader(fs, Encoding.UTF8);
                        //
                        string bas = sr.ReadLine();
                        string[] bits = bas.Split(' ');
                        n = int.Parse(bits[0]);
                        m = int.Parse(bits[1]);
                        k = int.Parse(bits[2]);
                        mod = int.Parse(bits[3]);

                        fYes = true;
                    }

                }
                catch (Exception)
                {
                    MessageBox.Show("Не вдалось відкрити файл!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    if (fs != null)
                        fs.Close();
                    if (sr != null )
                        sr.Close();
                }
                if (fYes)
                {
                    //
                }
            }
        }
    }
}
