﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy
{
    struct Data
    {
        public int state;
        public bool isCity;
        public int block;
        public Data(int state, bool isCity)
        {
            this.state = state;
            this.isCity = isCity;
            block = 0;
        }
        public Data(int state)
        {
            this.state = state;
            isCity = false;
            block = 0;
        }
    }

    public partial class Base : Form
    {

        // The height of a hexagon.
        private const float HexHeight = 30;
        // Selected hexagons.
        private Dictionary<PointF, Data> Hexagons = new Dictionary<PointF, Data>();
        private StartForm parent;
        private PointF[] moves = new PointF[6];
        private PointF[] movesN = new PointF[6];
        private int countMove;
        private int n, m, k, mod;

        public Base(StartForm start, int n, int m, int k, int mod)
        {
            InitializeComponent();
            parent = start;
            this.n = n;
            this.m = m;
            this.k = k;
            this.mod = mod;
            moves[0] = new PointF(-1, 0);
            moves[1] = new PointF(-1, 1);
            moves[2] = new PointF(0, 1);
            moves[3] = new PointF(1, 0);
            moves[4] = new PointF(0, -1);
            moves[5] = new PointF(-1, -1);
            //
            movesN[0] = new PointF(-1, 0);
            movesN[1] = new PointF(0, 1);
            movesN[2] = new PointF(1, 1);
            movesN[3] = new PointF(1, 0);
            movesN[4] = new PointF(1, -1);
            movesN[5] = new PointF(0, -1);

            countMove = 0;
            PointF[] width = DrawFields.HexToPoints(HexHeight, n, m);
            PointF[] height;
            if ( m % 2 != 0 )
            {
                height = DrawFields.HexToPoints(HexHeight, n, m - 1);
            }else
            {
                height = DrawFields.HexToPoints(HexHeight, n, m);
            }
            int w = Convert.ToInt32(width[1].X);
            int h = Convert.ToInt32(height[3].Y + 1);
            ground.Size = new Size(w, h);
            counterMove.Location = new Point(ground.Location.X + (ground.Width - counterMove.Width) / 2, ground.Location.Y + h + 5);
        }

        // Redraw the grid.
        public void ground_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            // Draw the selected hexagons.
            foreach (KeyValuePair<PointF, Data> point in Hexagons)
            {
                if ( point.Value.isCity )
                {
                    PointF[] points = DrawFields.HexToPoints(HexHeight,point.Key.X, point.Key.Y);
                    float x = (points[0].X + points[3].X) / 2;
                    float y = (points[1].Y + points[4].Y) / 2;
                    //e.Graphics.DrawImage(Properties.Resources.castle, x, y);
                    e.Graphics.DrawString("1", this.Font, Brushes.Black, x, y);
                }
                switch (point.Value.state)
                {
                    case 1:
                        e.Graphics.FillPolygon(Brushes.Purple,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case 2:
                        e.Graphics.FillPolygon(Brushes.LightGreen,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case -1:
                        e.Graphics.FillPolygon(Brushes.Aqua,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case -2:
                        e.Graphics.FillPolygon(Brushes.BurlyWood,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    default:
                        break;
                }
            }

            DrawFields.DrawHexGrid(e.Graphics, Pens.Black, n, m, HexHeight);
        }
        private void ground_Resize(object sender, EventArgs e)
        {
            ground.Refresh();
        }

        // Display the row and column under the mouse.
        private void ground_MouseMove(object sender, MouseEventArgs e)
        {
            int row, col;
            DrawFields.PointToHex(e.X, e.Y, HexHeight, out row, out col);
            //Console.WriteLine("(" + row + ", " + col + ")");
        }

        private void counterMove_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Color.LightGray);
            string s = countMove + "/" + k;
            Font drawFont = new Font("Arial", 8, FontStyle.Bold);
            Brush drawBrush = Brushes.Indigo;
            PointF pf = new PointF((counterMove.Width) / 2 - s.Length * 3, counterMove.Height / 2 - 8);
            g.DrawString(s, drawFont, drawBrush, pf);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*Console.WriteLine();
            foreach(MyPoint p in Hexagons)
            {
                string s = "(" + p.coord.X + ", " + p.coord.Y + ")" + " State: " + p.state + ";City: " + p.isCity;
                Console.WriteLine(s);
            }*/
        }

        // Add the clicked hexagon to the Hexagons list.
        private void ground_MouseClick(object sender, MouseEventArgs e)
        {
            if (countMove < k)
            {
                int row, col;
                DrawFields.PointToHex(e.X, e.Y, HexHeight, out row, out col);
                PointF here = new PointF(row, col);
                Data data = new Data();
                try
                {
                    if (Hexagons.TryGetValue(here, out data))
                    {
                        if (data.isCity == true && data.state != (countMove % 2 + 1))
                            throw new Exception("Хід заблоковано!Тут ворожа фортеця!");

                        if (data.isCity == true)
                            throw new Exception("Хід заблоковано!Навіщо захоплювати свою фортецю?:)");

                        if ( data.block == 1  )
                            throw new Exception("Хід заблоковано!Місто не може бути в морі!");

                        if ( data.block == 2 )
                            throw new Exception("Хід заблоковано!Місто не може бути у горах!");
                        Hexagons.Remove(here);
                        data.state = countMove % 2 + 1;
                    }
                    else
                    {
                        data.isCity = true;
                        data.state = countMove % 2 + 1;
                    }

                    if ((row >= 0 && row < n) && (col >= 0 && col < m))
                    {
                        Hexagons.Add(new PointF(row, col), data);
                        string[] str = { row.ToString(), col.ToString(), data.state == 1 ? "Перший" : "Другий" };
                        history.Rows.Add(str);
                        if (col % 2 == 0)
                        {
                            makeMove(moves, row, col);
                        }
                        else
                        {
                            makeMove(movesN, row, col);
                        }
                        countMove++;
                    }
                    counterMove.Refresh();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
        }

        private void Base_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Show();
        }

        private void makeMove(PointF[] moves, int row, int col)
        {
            foreach (PointF pf in moves)
            {

                float x = row + pf.X;
                float y = col + pf.Y;
                if ((x >= 0 && x < n) && (y >= 0 && y < m))
                {
                    Data data = new Data();
                    PointF pp = new PointF(x, y);

                    if ( Hexagons.TryGetValue(pp, out data) )
                    {
                        Hexagons.Remove(pp);
                    }

                    data.state = countMove % 2 + 1;
                    Hexagons.Add(pp, data);

                    ground.Refresh();
                    System.Threading.Thread.Sleep(50);
                }
            }
        }
    }

}
