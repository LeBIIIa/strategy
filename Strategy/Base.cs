﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Strategy
{
    struct Data
    {
        public int state;
        public bool isCity;
        public int block;
        public Data(int state, bool isCity)
        {
            this.state = state;
            this.isCity = isCity;
            block = 0;
        }
        public Data(int state)
        {
            this.state = state;
            isCity = false;
            block = 0;
        }
    }

    public partial class Base : Form
    {
        private const float HexHeight = 35;

        private Dictionary<PointF, Data> Hexagons = new Dictionary<PointF, Data>();
        private Dictionary<PointF, Data> bases = new Dictionary<PointF, Data>();

        private StartForm parent;

        private PointF[] moves = new PointF[6];
        private PointF[] movesN = new PointF[6];

        private int countMove;
        private int n, m, k, mod;
        private bool isBase = true;

        private DateTime initTime = DateTime.Now;
        private TimeSpan tick;
        

        public Base(StartForm start, int n, int m, int k, int mod)
        {
            InitializeComponent();
            parent = start;
            this.n = n;
            this.m = m;
            this.k = k;
            this.mod = mod;
            moves[0] = new PointF(-1, 0);
            moves[1] = new PointF(-1, 1);
            moves[2] = new PointF(0, 1);
            moves[3] = new PointF(1, 0);
            moves[4] = new PointF(0, -1);
            moves[5] = new PointF(-1, -1);
            //
            movesN[0] = new PointF(-1, 0);
            movesN[1] = new PointF(0, 1);
            movesN[2] = new PointF(1, 1);
            movesN[3] = new PointF(1, 0);
            movesN[4] = new PointF(1, -1);
            movesN[5] = new PointF(0, -1);

            countMove = 0;
            PointF[] width = DrawFields.HexToPoints(HexHeight, n, m);
            PointF[] height;
            if ( m % 2 != 0 )
                height = DrawFields.HexToPoints(HexHeight, n, m - 1);
            else
                height = DrawFields.HexToPoints(HexHeight, n, m);

            int w = Convert.ToInt32(width[1].X);
            int h = Convert.ToInt32(height[3].Y + 1);
            ground.Size = new Size(w, h);
            counterMove.Location = new Point(ground.Location.X + (ground.Width - counterMove.Width - btnHelp.Width - whichMove.Width) / 2, ground.Location.Y + h + 5);
            w = counterMove.Location.X + counterMove.Width;
            whichMove.Location = new Point(w + (ground.Location.X + ground.Width - w - whichMove.Width - btnHelp.Width) / 2, ground.Location.Y + h + 5);
            btnHelp.Location = new Point(ground.Location.X + ground.Width - btnHelp.Width, ground.Location.Y + h + 5);
            history.Size = new Size(history.Width, h);
            play.Location = new Point(history.Location.X + (history.Width - play.Width) / 2, history.Location.Y + h + 5);
            createBaseCity();
            
        }
        
        public void ground_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            
            foreach (KeyValuePair<PointF, Data> point in Hexagons)
            {
                switch (point.Value.state)
                {
                    case 1:
                        e.Graphics.FillPolygon(Brushes.LightCoral,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case 2:
                        e.Graphics.FillPolygon(Brushes.LightGreen,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case -1:
                        e.Graphics.FillPolygon(Brushes.Aqua,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    case -2:
                        e.Graphics.FillPolygon(Brushes.BurlyWood,
                            DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y));
                        break;
                    default:
                        break;
                }
                if (point.Value.isCity)
                {
                    PointF[] points = DrawFields.HexToPoints(HexHeight, point.Key.X, point.Key.Y);
                    int x = Convert.ToInt32(points[0].X + (HexHeight - 12) / 2 / Math.Sqrt(3));
                    int y = Convert.ToInt32(points[1].Y + 2);
                    Image image = null;
                    if (point.Value.state == 1)
                        image = Properties.Resources.castle_red;
                    else if (point.Value.state == 2)
                        image = Properties.Resources.castle_green;
                    e.Graphics.DrawImage(image, x, y);
                }
            }


            DrawFields.DrawHexGrid(e.Graphics, Pens.Black, n, m, HexHeight);

        }
        private void ground_Resize(object sender, EventArgs e)
        {
            ground.Refresh();
        }

        private void counterMove_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Color.LightGray);
            string s = countMove + "/" + k;
            Font drawFont = new Font("Arial", 8, FontStyle.Bold);
            Brush drawBrush = Brushes.Indigo;
            PointF pf = new PointF((counterMove.Width) / 2 - s.Length * 3, counterMove.Height / 2 - 8);
            g.DrawString(s, drawFont, drawBrush, pf);
        }

        private void play_Click(object sender, EventArgs e)
        {
            try
            {
                int count = history.SelectedRows[0].Index + 1;
                int i = 0;
                if (count == countMove)
                    return;
                if ( count < countMove )
                {
                    Hexagons.Clear();
                    createBaseCity();
                    countMove = 0;
                }else
                {
                    i = countMove;
                }
                for (;i<count;++i)
                {
                    DataGridViewRow row = history.Rows[i];
                    PointF p = new PointF(float.Parse(row.Cells[0].Value.ToString(), CultureInfo.InvariantCulture.NumberFormat), 
                        float.Parse(row.Cells[1].Value.ToString(), CultureInfo.InvariantCulture.NumberFormat));
                    p.X--;
                    p.Y--;
                    Data data = new Data();
                    if (row.Cells[2].Value.ToString().CompareTo("Красний") == 0)
                        data.state = 1;
                    else
                        data.state = 2;
                    if (row.Cells[3].Value.ToString().CompareTo("True") == 0)
                        data.isCity = true;
                    else
                        data.isCity = false;
                    data.block = Convert.ToInt32(row.Cells[4].Value.ToString());
                    addCity((int)p.X, (int)p.Y, data, false);
                }
                ground.Refresh();

            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Виберіть хід!", "Помилка", MessageBoxButtons.OK);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void ground_MouseClick(object sender, MouseEventArgs e)
        {
            if (countMove < k)
            {
                int row, col;
                DrawFields.PointToHex(e.X, e.Y, HexHeight, out row, out col);
            
                try
                {
                    addCity(row, col, new Data(), true);
                    endGame();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Base_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime current_time = DateTime.Now;
            tick = current_time - initTime;
        }

        private void endGame()
        {
            if (k == countMove)
            {
                int pl1, pl2;
                int csPl1, csPl2, sPl1, mPl1, sPl2, mPl2;
                pl1 = pl2 = csPl1 = csPl2 = sPl1 = mPl1 = sPl2 = mPl2 = 0;
                foreach (KeyValuePair<PointF, Data> point in Hexagons)
                {
                    if (point.Value.state == 1)
                    {
                        pl1++;
                        if (point.Value.isCity)
                            csPl1++;
                        if (point.Value.block == 1)
                            sPl1++;
                        if (point.Value.block == 2)
                            mPl1++;
                    }
                    if (point.Value.state == 2)
                    {
                        pl2++;
                        if (point.Value.isCity)
                            csPl2++;
                        if (point.Value.block == 1)
                            sPl2++;
                        if (point.Value.block == 2)
                            mPl2++;
                    }
                }
                Stat myStat = new Stat(pl1, csPl1, sPl1, mPl1, pl2, csPl2, sPl2, mPl2, tick);
                myStat.ShowDialog();
                play.Enabled = true;
            }
        }

        private void createBaseCity()
        {
            if (bases.Count == 0)
            {
                Random rand = new Random();
                int rowf = rand.Next(0, n);
                int colf = 0;
                addCity(rowf, colf, new Data(), false);
                int rows = rand.Next(0, n);
                int cols = m - 1;
                addCity(rows, cols, new Data(), false);
                countMove = 0;
                counterMove.Refresh();
                isBase = false;
            }
            foreach (KeyValuePair<PointF, Data> point in bases)
            {
                Hexagons.Add(point.Key, point.Value);
            }
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + @"\help.pdf";
            FileInfo inf = new FileInfo(path);
            if (inf.Exists)
                System.Diagnostics.Process.Start(path);
            else
                MessageBox.Show("Не знайдено help.pdf", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void whichMove_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Color.LightGray);
            string s;
            Brush drawBrush;
            Font drawFont = new Font("Arial", 8, FontStyle.Bold);
            if ( (countMove % 2 + 1 ) == 1  )
            {
                s = "Красний";
                drawBrush = Brushes.Red;
                PointF pf = new PointF((whichMove.Width - s.Length * 8) / 2, whichMove.Height / 2 - drawFont.Size);
                g.DrawString(s, drawFont, drawBrush, pf);
            }else if ( (countMove % 2 + 1) == 2 )
            {
                s = "Зелений";
                drawBrush = Brushes.Green;
                PointF pf = new PointF((whichMove.Width - s.Length * 8) / 2, whichMove.Height / 2 - drawFont.Size);
                g.DrawString(s, drawFont, drawBrush, pf);

            }
        }

        private void addCity(int row, int col, Data data, bool flag)
        {
            try
            {
                if ((row >= 0 && row < n) && (col >= 0 && col < m))
                {
                    PointF here = new PointF(row, col);

                    tryGet(here, out data);

                    data.isCity = true;
                    data.state = countMove % 2 + 1;

                    if (isBase == true)
                        bases.Add(new PointF(row, col), data);
                    else
                        Hexagons.Add(new PointF(row, col), data);

                    if (flag == true)
                    {
                        string[] str = { (row + 1).ToString(), (col + 1).ToString(), data.state == 1 ? "Красний" : "Зелений", data.isCity.ToString(), data.block.ToString() };
                        history.Rows.Add(str);
                    }

                    if (col % 2 == 0)
                        makeMove(moves, row, col);
                    else
                        makeMove(movesN, row, col);

                    countMove++;
                    counterMove.Refresh();
                    whichMove.Refresh();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       }

        private void tryGet(PointF p, out Data data)
        {
            data = new Data();
            if (Hexagons.TryGetValue(p, out data))
            {
                if (data.isCity == true && data.state != (countMove % 2 + 1))
                    throw new Exception("Хід заблоковано!Тут ворожа фортеця!");

                if (data.isCity == true)
                    throw new Exception("Хід заблоковано!Навіщо захоплювати свою фортецю?:)");

                if (data.block == 1)
                    throw new Exception("Хід заблоковано!Місто не може бути в морі!");

                if (data.block == 2)
                    throw new Exception("Хід заблоковано!Місто не може бути у горах!");
                Hexagons.Remove(p);
            }
        }

        private void makeMove(PointF[] moves, int row, int col)
        {
            foreach (PointF pf in moves)
            {
                float x = row + pf.X;
                float y = col + pf.Y;
                if ((x >= 0 && x < n) && (y >= 0 && y < m))
                {
                    Data data = new Data();
                    PointF pp = new PointF(x, y);

                    if ( Hexagons.TryGetValue(pp, out data) )
                        Hexagons.Remove(pp);

                    data.state = countMove % 2 + 1;
                    if (isBase == true)
                        bases.Add(pp, data);
                    else
                        Hexagons.Add(pp, data);

                    ground.Refresh();
                    System.Threading.Thread.Sleep(50);
                }
            }
        }
    }

}
