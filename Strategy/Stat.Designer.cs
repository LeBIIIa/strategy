﻿namespace Strategy
{
    partial class Stat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.FirstPlayer = new System.Windows.Forms.Label();
            this.SecondPlayer = new System.Windows.Forms.Label();
            this.countAreas = new System.Windows.Forms.Label();
            this.countCastle = new System.Windows.Forms.Label();
            this.countMountains = new System.Windows.Forms.Label();
            this.countSea = new System.Windows.Forms.Label();
            this.FirstArea = new System.Windows.Forms.Label();
            this.SecondArea = new System.Windows.Forms.Label();
            this.FirstCastle = new System.Windows.Forms.Label();
            this.SecondCastle = new System.Windows.Forms.Label();
            this.FirstMountains = new System.Windows.Forms.Label();
            this.FirstSea = new System.Windows.Forms.Label();
            this.SecondMountains = new System.Windows.Forms.Label();
            this.SecondSea = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(291, 261);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4, 3, 4, 12);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 27);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "ОК";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // FirstPlayer
            // 
            this.FirstPlayer.AutoSize = true;
            this.FirstPlayer.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstPlayer.ForeColor = System.Drawing.Color.Red;
            this.FirstPlayer.Location = new System.Drawing.Point(258, 11);
            this.FirstPlayer.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.FirstPlayer.Name = "FirstPlayer";
            this.FirstPlayer.Size = new System.Drawing.Size(119, 17);
            this.FirstPlayer.TabIndex = 2;
            this.FirstPlayer.Text = "Красний гравець";
            this.FirstPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondPlayer
            // 
            this.SecondPlayer.AutoSize = true;
            this.SecondPlayer.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondPlayer.ForeColor = System.Drawing.Color.Green;
            this.SecondPlayer.Location = new System.Drawing.Point(389, 12);
            this.SecondPlayer.Margin = new System.Windows.Forms.Padding(6, 0, 12, 0);
            this.SecondPlayer.Name = "SecondPlayer";
            this.SecondPlayer.Size = new System.Drawing.Size(121, 17);
            this.SecondPlayer.TabIndex = 3;
            this.SecondPlayer.Text = "Зелений гравець";
            this.SecondPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countAreas
            // 
            this.countAreas.AutoSize = true;
            this.countAreas.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countAreas.Location = new System.Drawing.Point(16, 43);
            this.countAreas.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.countAreas.Name = "countAreas";
            this.countAreas.Size = new System.Drawing.Size(216, 17);
            this.countAreas.TabIndex = 4;
            this.countAreas.Text = "Кількість захоплених територій:";
            this.countAreas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countCastle
            // 
            this.countCastle.AutoSize = true;
            this.countCastle.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countCastle.Location = new System.Drawing.Point(16, 77);
            this.countCastle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.countCastle.Name = "countCastle";
            this.countCastle.Size = new System.Drawing.Size(129, 17);
            this.countCastle.TabIndex = 5;
            this.countCastle.Text = "Кількість фортець:";
            this.countCastle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countMountains
            // 
            this.countMountains.AutoSize = true;
            this.countMountains.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countMountains.Location = new System.Drawing.Point(16, 105);
            this.countMountains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.countMountains.Name = "countMountains";
            this.countMountains.Size = new System.Drawing.Size(168, 17);
            this.countMountains.TabIndex = 6;
            this.countMountains.Text = "Кількість захоплених гір:";
            this.countMountains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countSea
            // 
            this.countSea.AutoSize = true;
            this.countSea.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countSea.Location = new System.Drawing.Point(16, 142);
            this.countSea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.countSea.Name = "countSea";
            this.countSea.Size = new System.Drawing.Size(251, 17);
            this.countSea.TabIndex = 7;
            this.countSea.Text = "Кількість захоплених територій моря:";
            this.countSea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FirstArea
            // 
            this.FirstArea.AutoSize = true;
            this.FirstArea.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstArea.Location = new System.Drawing.Point(338, 43);
            this.FirstArea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FirstArea.Name = "FirstArea";
            this.FirstArea.Size = new System.Drawing.Size(16, 17);
            this.FirstArea.TabIndex = 8;
            this.FirstArea.Text = "0";
            this.FirstArea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondArea
            // 
            this.SecondArea.AutoSize = true;
            this.SecondArea.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondArea.Location = new System.Drawing.Point(452, 43);
            this.SecondArea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SecondArea.Name = "SecondArea";
            this.SecondArea.Size = new System.Drawing.Size(16, 17);
            this.SecondArea.TabIndex = 9;
            this.SecondArea.Text = "0";
            this.SecondArea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FirstCastle
            // 
            this.FirstCastle.AutoSize = true;
            this.FirstCastle.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstCastle.Location = new System.Drawing.Point(338, 77);
            this.FirstCastle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FirstCastle.Name = "FirstCastle";
            this.FirstCastle.Size = new System.Drawing.Size(16, 17);
            this.FirstCastle.TabIndex = 10;
            this.FirstCastle.Text = "0";
            this.FirstCastle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondCastle
            // 
            this.SecondCastle.AutoSize = true;
            this.SecondCastle.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondCastle.Location = new System.Drawing.Point(452, 77);
            this.SecondCastle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SecondCastle.Name = "SecondCastle";
            this.SecondCastle.Size = new System.Drawing.Size(16, 17);
            this.SecondCastle.TabIndex = 11;
            this.SecondCastle.Text = "0";
            this.SecondCastle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FirstMountains
            // 
            this.FirstMountains.AutoSize = true;
            this.FirstMountains.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstMountains.Location = new System.Drawing.Point(338, 105);
            this.FirstMountains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FirstMountains.Name = "FirstMountains";
            this.FirstMountains.Size = new System.Drawing.Size(16, 17);
            this.FirstMountains.TabIndex = 12;
            this.FirstMountains.Text = "0";
            this.FirstMountains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FirstSea
            // 
            this.FirstSea.AutoSize = true;
            this.FirstSea.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FirstSea.Location = new System.Drawing.Point(338, 142);
            this.FirstSea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FirstSea.Name = "FirstSea";
            this.FirstSea.Size = new System.Drawing.Size(16, 17);
            this.FirstSea.TabIndex = 13;
            this.FirstSea.Text = "0";
            this.FirstSea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondMountains
            // 
            this.SecondMountains.AutoSize = true;
            this.SecondMountains.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondMountains.Location = new System.Drawing.Point(452, 105);
            this.SecondMountains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SecondMountains.Name = "SecondMountains";
            this.SecondMountains.Size = new System.Drawing.Size(16, 17);
            this.SecondMountains.TabIndex = 13;
            this.SecondMountains.Text = "0";
            this.SecondMountains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SecondSea
            // 
            this.SecondSea.AutoSize = true;
            this.SecondSea.Font = new System.Drawing.Font("Showcard Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SecondSea.Location = new System.Drawing.Point(452, 142);
            this.SecondSea.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.SecondSea.Name = "SecondSea";
            this.SecondSea.Size = new System.Drawing.Size(16, 17);
            this.SecondSea.TabIndex = 14;
            this.SecondSea.Text = "0";
            this.SecondSea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time.Location = new System.Drawing.Point(16, 12);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(31, 16);
            this.time.TabIndex = 15;
            this.time.Text = "Час:";
            // 
            // Stat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(531, 301);
            this.Controls.Add(this.time);
            this.Controls.Add(this.SecondSea);
            this.Controls.Add(this.SecondMountains);
            this.Controls.Add(this.FirstSea);
            this.Controls.Add(this.FirstMountains);
            this.Controls.Add(this.SecondCastle);
            this.Controls.Add(this.FirstCastle);
            this.Controls.Add(this.SecondArea);
            this.Controls.Add(this.FirstArea);
            this.Controls.Add(this.countSea);
            this.Controls.Add(this.countMountains);
            this.Controls.Add(this.countCastle);
            this.Controls.Add(this.countAreas);
            this.Controls.Add(this.SecondPlayer);
            this.Controls.Add(this.FirstPlayer);
            this.Controls.Add(this.btnOk);
            this.Font = new System.Drawing.Font("Script MT Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Stat";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Статистика";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label FirstPlayer;
        private System.Windows.Forms.Label SecondPlayer;
        private System.Windows.Forms.Label countAreas;
        private System.Windows.Forms.Label countCastle;
        private System.Windows.Forms.Label countMountains;
        private System.Windows.Forms.Label countSea;
        private System.Windows.Forms.Label FirstArea;
        private System.Windows.Forms.Label SecondArea;
        private System.Windows.Forms.Label FirstCastle;
        private System.Windows.Forms.Label SecondCastle;
        private System.Windows.Forms.Label FirstMountains;
        private System.Windows.Forms.Label FirstSea;
        private System.Windows.Forms.Label SecondMountains;
        private System.Windows.Forms.Label SecondSea;
        private System.Windows.Forms.Label time;
    }
}