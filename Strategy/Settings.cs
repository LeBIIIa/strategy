﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy
{
    public partial class moves : Form
    {
        int n, m, k, mod;
        public moves()
        {
            InitializeComponent();
            mode.SelectedIndex = 0;
        }

        private void save_Click(object sender, EventArgs e)
        {
            n = decimal.ToInt32(rowValue.Value);
            m = decimal.ToInt32(columnValue.Value);
            k = decimal.ToInt32(countMoves.Value);
            mod = mode.SelectedIndex + 1;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            countMoves.Maximum = (columnValue.Value * rowValue.Value) / 2;
        }

        public int Row
        {
            get { return n; }
        }

        public int Column
        {
            get { return m; }
        }

        public int CountMoves
        {
            get { return k; }
        }

        public int Mode
        {
            get { return mod; }
        }
    }
}
