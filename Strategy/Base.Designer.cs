﻿namespace Strategy
{
    partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Base));
            this.ground = new System.Windows.Forms.PictureBox();
            this.history = new System.Windows.Forms.DataGridView();
            this.play = new System.Windows.Forms.Button();
            this.counterMove = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.whichMove = new System.Windows.Forms.PictureBox();
            this.btnHelp = new System.Windows.Forms.Button();
            this.X = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CityImage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Blocked = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.history)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counterMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.whichMove)).BeginInit();
            this.SuspendLayout();
            // 
            // ground
            // 
            this.ground.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ground.Location = new System.Drawing.Point(141, 12);
            this.ground.Margin = new System.Windows.Forms.Padding(3, 3, 12, 3);
            this.ground.Name = "ground";
            this.ground.Size = new System.Drawing.Size(602, 315);
            this.ground.TabIndex = 0;
            this.ground.TabStop = false;
            this.ground.Paint += new System.Windows.Forms.PaintEventHandler(this.ground_Paint);
            this.ground.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ground_MouseClick);
            this.ground.Resize += new System.EventHandler(this.ground_Resize);
            // 
            // history
            // 
            this.history.AllowUserToAddRows = false;
            this.history.AllowUserToDeleteRows = false;
            this.history.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.history.ColumnHeadersVisible = false;
            this.history.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.X,
            this.Y,
            this.State,
            this.CityImage,
            this.Blocked});
            this.history.Location = new System.Drawing.Point(12, 12);
            this.history.MultiSelect = false;
            this.history.Name = "history";
            this.history.ReadOnly = true;
            this.history.RowHeadersVisible = false;
            this.history.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.history.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.history.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.history.Size = new System.Drawing.Size(123, 315);
            this.history.TabIndex = 1;
            // 
            // play
            // 
            this.play.Enabled = false;
            this.play.Location = new System.Drawing.Point(29, 333);
            this.play.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.play.Name = "play";
            this.play.Size = new System.Drawing.Size(90, 23);
            this.play.TabIndex = 2;
            this.play.Text = "Програти ходи";
            this.play.UseVisualStyleBackColor = true;
            this.play.Click += new System.EventHandler(this.play_Click);
            // 
            // counterMove
            // 
            this.counterMove.BackColor = System.Drawing.Color.White;
            this.counterMove.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.counterMove.Location = new System.Drawing.Point(336, 333);
            this.counterMove.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.counterMove.Name = "counterMove";
            this.counterMove.Size = new System.Drawing.Size(60, 23);
            this.counterMove.TabIndex = 3;
            this.counterMove.TabStop = false;
            this.counterMove.Paint += new System.Windows.Forms.PaintEventHandler(this.counterMove_Paint);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // whichMove
            // 
            this.whichMove.BackColor = System.Drawing.Color.White;
            this.whichMove.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.whichMove.Location = new System.Drawing.Point(439, 333);
            this.whichMove.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.whichMove.Name = "whichMove";
            this.whichMove.Size = new System.Drawing.Size(60, 23);
            this.whichMove.TabIndex = 4;
            this.whichMove.TabStop = false;
            this.whichMove.Paint += new System.Windows.Forms.PaintEventHandler(this.whichMove_Paint);
            // 
            // btnHelp
            // 
            this.btnHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnHelp.ForeColor = System.Drawing.Color.Red;
            this.btnHelp.Location = new System.Drawing.Point(718, 333);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(25, 23);
            this.btnHelp.TabIndex = 5;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // X
            // 
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = null;
            this.X.DefaultCellStyle = dataGridViewCellStyle1;
            this.X.FillWeight = 25F;
            this.X.HeaderText = "X";
            this.X.MinimumWidth = 25;
            this.X.Name = "X";
            this.X.ReadOnly = true;
            this.X.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.X.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.X.Width = 25;
            // 
            // Y
            // 
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.Y.DefaultCellStyle = dataGridViewCellStyle2;
            this.Y.FillWeight = 25F;
            this.Y.HeaderText = "Y";
            this.Y.MinimumWidth = 25;
            this.Y.Name = "Y";
            this.Y.ReadOnly = true;
            this.Y.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Y.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Y.Width = 25;
            // 
            // State
            // 
            this.State.FillWeight = 53F;
            this.State.HeaderText = "State";
            this.State.MinimumWidth = 53;
            this.State.Name = "State";
            this.State.ReadOnly = true;
            this.State.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.State.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.State.Width = 53;
            // 
            // CityImage
            // 
            dataGridViewCellStyle3.NullValue = null;
            this.CityImage.DefaultCellStyle = dataGridViewCellStyle3;
            this.CityImage.HeaderText = "CityImage";
            this.CityImage.Name = "CityImage";
            this.CityImage.ReadOnly = true;
            this.CityImage.Visible = false;
            // 
            // Blocked
            // 
            this.Blocked.HeaderText = "Blocked";
            this.Blocked.Name = "Blocked";
            this.Blocked.ReadOnly = true;
            this.Blocked.Visible = false;
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(753, 364);
            this.Controls.Add(this.btnHelp);
            this.Controls.Add(this.whichMove);
            this.Controls.Add(this.counterMove);
            this.Controls.Add(this.play);
            this.Controls.Add(this.history);
            this.Controls.Add(this.ground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Base";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Стратегічна гра";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Base_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.history)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counterMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.whichMove)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ground;
        private System.Windows.Forms.DataGridView history;
        private System.Windows.Forms.Button play;
        private System.Windows.Forms.PictureBox counterMove;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox whichMove;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.DataGridViewTextBoxColumn X;
        private System.Windows.Forms.DataGridViewTextBoxColumn Y;
        private System.Windows.Forms.DataGridViewTextBoxColumn State;
        private System.Windows.Forms.DataGridViewTextBoxColumn CityImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn Blocked;
    }
}

