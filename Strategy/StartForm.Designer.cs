﻿namespace Strategy
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.byMouse = new System.Windows.Forms.Button();
            this.byFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // byMouse
            // 
            this.byMouse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.byMouse.BackColor = System.Drawing.Color.Transparent;
            this.byMouse.FlatAppearance.BorderSize = 0;
            this.byMouse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.byMouse.Location = new System.Drawing.Point(98, -1);
            this.byMouse.Name = "byMouse";
            this.byMouse.Size = new System.Drawing.Size(100, 50);
            this.byMouse.TabIndex = 0;
            this.byMouse.TabStop = false;
            this.byMouse.Text = "Грати мишкою";
            this.byMouse.UseVisualStyleBackColor = false;
            this.byMouse.Click += new System.EventHandler(this.byMouse_Click);
            // 
            // byFile
            // 
            this.byFile.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.byFile.BackColor = System.Drawing.Color.Transparent;
            this.byFile.Enabled = false;
            this.byFile.FlatAppearance.BorderSize = 0;
            this.byFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.byFile.Location = new System.Drawing.Point(-1, -1);
            this.byFile.Name = "byFile";
            this.byFile.Size = new System.Drawing.Size(100, 50);
            this.byFile.TabIndex = 1;
            this.byFile.TabStop = false;
            this.byFile.Text = "Зчитувати з файлу";
            this.byFile.UseVisualStyleBackColor = false;
            this.byFile.Click += new System.EventHandler(this.byFile_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(197, 48);
            this.Controls.Add(this.byFile);
            this.Controls.Add(this.byMouse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StartForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Почати гру";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button byMouse;
        private System.Windows.Forms.Button byFile;
    }
}