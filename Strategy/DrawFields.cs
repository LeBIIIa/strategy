﻿using System;
using System.Drawing;

namespace Strategy
{
    public static class DrawFields
    {
        public static void DrawHexGrid(Graphics gr, Pen pen,
            int row, int col, float height)
        {
            for (int i = 0; i<row; i++)
            {
                PointF[] points = HexToPoints(height, i, 0);
                
                for (int j = 0; j<col; j++)
                {
                    points = HexToPoints(height, i, j);

                    gr.DrawPolygon(pen, points);
                }
            }
                        
        }

       
        public static float HexWidth(float height)
        {
            return (float)(4 * (height / 2 / Math.Sqrt(3)));
        }
        
        public static void PointToHex(float x, float y, float height,
            out int row, out int col)
        {
            float width = HexWidth(height);
            col = (int)(x / (width * 0.75f));

            if (col % 2 == 0)
                row = (int)(y / height);
            else
                row = (int)((y - height / 2) / height);

            float testx = col * width * 0.75f;
            float testy = row * height;
            if (col % 2 == 1) testy += height / 2;

            bool is_above = false, is_below = false;
            float dx = x - testx;
            if (dx < width / 4)
            {
                float dy = y - (testy + height / 2);
                if (dx < 0.001)
                {
                    
                    if (dy < 0) is_above = true;
                    if (dy > 0) is_below = true;
                }
                else if (dy < 0)
                {
                    if (-dy / dx > Math.Sqrt(3)) is_above = true;
                }
                else
                {
                    if (dy / dx > Math.Sqrt(3)) is_below = true;
                }
            }
            
            if (is_above)
            {
                if (col % 2 == 0) row--;
                col--;
            }
            else if (is_below)
            {
                if (col % 2 == 1) row++;
                col--;
            }
        }
        
        public static PointF[] HexToPoints(float height, float row, float col)
        {
            float width = HexWidth(height);
            float y = height / 2;
            float x = 0;
            
            y += row * height;
            
            if (col % 2 == 1) y += height / 2;
            
            x += col * (width * 0.75f);
            
            return new PointF[]
                {
                    new PointF(x, y),
                    new PointF(x + width * 0.25f, y - height / 2),
                    new PointF(x + width * 0.75f, y - height / 2),
                    new PointF(x + width, y),
                    new PointF(x + width * 0.75f, y + height / 2),
                    new PointF(x + width * 0.25f, y + height / 2),
                };
        }
    }
}
