﻿namespace Strategy
{
    partial class moves
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.row = new System.Windows.Forms.Label();
            this.column = new System.Windows.Forms.Label();
            this.rowValue = new System.Windows.Forms.NumericUpDown();
            this.columnValue = new System.Windows.Forms.NumericUpDown();
            this.save = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.countMoves = new System.Windows.Forms.NumericUpDown();
            this.mode = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.rowValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.countMoves)).BeginInit();
            this.SuspendLayout();
            // 
            // row
            // 
            this.row.AutoSize = true;
            this.row.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.row.Location = new System.Drawing.Point(13, 12);
            this.row.Name = "row";
            this.row.Size = new System.Drawing.Size(136, 19);
            this.row.TabIndex = 0;
            this.row.Text = "Кількість рядків:";
            // 
            // column
            // 
            this.column.AutoSize = true;
            this.column.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.column.Location = new System.Drawing.Point(13, 41);
            this.column.Name = "column";
            this.column.Size = new System.Drawing.Size(158, 19);
            this.column.TabIndex = 1;
            this.column.Text = "Кількість стовпців:";
            // 
            // rowValue
            // 
            this.rowValue.Location = new System.Drawing.Point(170, 12);
            this.rowValue.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.rowValue.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rowValue.Name = "rowValue";
            this.rowValue.Size = new System.Drawing.Size(57, 20);
            this.rowValue.TabIndex = 3;
            this.rowValue.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.rowValue.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // columnValue
            // 
            this.columnValue.Location = new System.Drawing.Point(170, 41);
            this.columnValue.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.columnValue.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.columnValue.Name = "columnValue";
            this.columnValue.Size = new System.Drawing.Size(57, 20);
            this.columnValue.TabIndex = 4;
            this.columnValue.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.columnValue.ValueChanged += new System.EventHandler(this.ValueChanged);
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(12, 131);
            this.save.Margin = new System.Windows.Forms.Padding(3, 3, 3, 12);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 5;
            this.save.Text = "Зберегти";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(152, 131);
            this.cancel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 12);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 6;
            this.cancel.Text = "Відмінити";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(14, 67);
            this.label1.MaximumSize = new System.Drawing.Size(140, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 34);
            this.label1.TabIndex = 7;
            this.label1.Text = "Кількість ходів (один гравець)";
            // 
            // countMoves
            // 
            this.countMoves.Location = new System.Drawing.Point(170, 67);
            this.countMoves.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.countMoves.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.countMoves.Name = "countMoves";
            this.countMoves.Size = new System.Drawing.Size(57, 20);
            this.countMoves.TabIndex = 8;
            this.countMoves.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // mode
            // 
            this.mode.Enabled = false;
            this.mode.FormattingEnabled = true;
            this.mode.Items.AddRange(new object[] {
            "I’m too young to die",
            "Hey, not too rough!",
            "Hurt me plenty"});
            this.mode.Location = new System.Drawing.Point(11, 104);
            this.mode.Name = "mode";
            this.mode.Size = new System.Drawing.Size(216, 21);
            this.mode.TabIndex = 9;
            // 
            // moves
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(239, 164);
            this.Controls.Add(this.mode);
            this.Controls.Add(this.countMoves);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.save);
            this.Controls.Add(this.columnValue);
            this.Controls.Add(this.rowValue);
            this.Controls.Add(this.column);
            this.Controls.Add(this.row);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "moves";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки гри";
            ((System.ComponentModel.ISupportInitialize)(this.rowValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.countMoves)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label row;
        private System.Windows.Forms.Label column;
        private System.Windows.Forms.NumericUpDown rowValue;
        private System.Windows.Forms.NumericUpDown columnValue;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown countMoves;
        private System.Windows.Forms.ComboBox mode;
    }
}