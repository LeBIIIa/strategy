﻿namespace Strategy
{
    partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ground = new System.Windows.Forms.PictureBox();
            this.history = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.counterMove = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.history)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counterMove)).BeginInit();
            this.SuspendLayout();
            // 
            // ground
            // 
            this.ground.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ground.Location = new System.Drawing.Point(156, 12);
            this.ground.Name = "ground";
            this.ground.Size = new System.Drawing.Size(756, 684);
            this.ground.TabIndex = 0;
            this.ground.TabStop = false;
            this.ground.Paint += new System.Windows.Forms.PaintEventHandler(this.ground_Paint);
            this.ground.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ground_MouseClick);
            this.ground.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ground_MouseMove);
            this.ground.Resize += new System.EventHandler(this.ground_Resize);
            // 
            // history
            // 
            this.history.AllowUserToAddRows = false;
            this.history.AllowUserToDeleteRows = false;
            this.history.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.history.ColumnHeadersVisible = false;
            this.history.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.history.Location = new System.Drawing.Point(12, 12);
            this.history.MultiSelect = false;
            this.history.Name = "history";
            this.history.ReadOnly = true;
            this.history.RowHeadersVisible = false;
            this.history.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.history.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.history.Size = new System.Drawing.Size(138, 684);
            this.history.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.FillWeight = 25F;
            this.Column1.HeaderText = "Column1";
            this.Column1.MinimumWidth = 25;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 25;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 25F;
            this.Column2.HeaderText = "Column2";
            this.Column2.MinimumWidth = 25;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 25;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 50F;
            this.Column3.HeaderText = "Column3";
            this.Column3.MinimumWidth = 50;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 702);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // counterMove
            // 
            this.counterMove.BackColor = System.Drawing.Color.White;
            this.counterMove.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.counterMove.Location = new System.Drawing.Point(417, 702);
            this.counterMove.Name = "counterMove";
            this.counterMove.Size = new System.Drawing.Size(70, 23);
            this.counterMove.TabIndex = 3;
            this.counterMove.TabStop = false;
            this.counterMove.Paint += new System.Windows.Forms.PaintEventHandler(this.counterMove_Paint);
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 728);
            this.Controls.Add(this.counterMove);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.history);
            this.Controls.Add(this.ground);
            this.Name = "Base";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Base_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.history)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counterMove)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ground;
        private System.Windows.Forms.DataGridView history;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox counterMove;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}

