﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Strategy
{
    public partial class Stat : Form
    {
        public Stat(int pl1, int csPl1, int sPl1, int mPl1, int pl2, int csPl2, int sPl2, int mPl2, TimeSpan tick)
        {
            InitializeComponent();
            FirstPlayer.Location = new Point(FirstPlayer.Location.X, 12);
            SecondPlayer.Location = new Point(SecondPlayer.Location.X, 12);
            countAreas.Location = new Point(countAreas.Location.X, 3*12);
            countCastle.Location = new Point(countCastle.Location.X, countAreas.Location.Y + countAreas.Height + 12);
            countMountains.Location = new Point(countMountains.Location.X, countCastle.Location.Y + countCastle.Height + 12);
            countSea.Location = new Point(countSea.Location.X, countMountains.Location.Y + countMountains.Height + 12);
            if (sPl1 == 0 && sPl2 == 0)
            {
                countSea.Visible = false;
                FirstSea.Visible = false;
                SecondSea.Visible = false;
            }
            if (mPl1 == 0 && mPl2 == 0)
            {
                countMountains.Visible = false;
                FirstMountains.Visible = false;
                SecondMountains.Visible = false;
            }
            //First
            FirstArea.Text = pl1.ToString();
            FirstCastle.Text = csPl1.ToString();
            FirstMountains.Text = mPl1.ToString();
            FirstSea.Text = sPl1.ToString();

            FirstArea.Location = new Point(FirstPlayer.Location.X + (FirstPlayer.Width - FirstArea.Width) / 2, 3*12);
            FirstCastle.Location = new Point(FirstPlayer.Location.X + (FirstPlayer.Width - FirstCastle.Width) / 2, FirstArea.Location.Y + FirstArea.Height + 12);
            FirstMountains.Location = new Point(FirstPlayer.Location.X + (FirstPlayer.Width - FirstMountains.Width) / 2, FirstCastle.Location.Y + FirstCastle.Height + 12);
            FirstSea.Location = new Point(FirstPlayer.Location.X + (FirstPlayer.Width - FirstSea.Width) / 2, FirstMountains.Location.Y + FirstMountains.Height + 12);
            //Second
            SecondArea.Text = pl2.ToString();
            SecondCastle.Text = csPl2.ToString();
            SecondMountains.Text = mPl2.ToString();
            SecondSea.Text = sPl2.ToString();

            SecondArea.Location = new Point(SecondPlayer.Location.X + (SecondPlayer.Width - SecondArea.Width) / 2, 3*12);
            SecondCastle.Location = new Point(SecondArea.Location.X + (SecondArea.Width - SecondCastle.Width) / 2, SecondArea.Location.Y + SecondArea.Height + 12);
            SecondMountains.Location = new Point(SecondArea.Location.X + (SecondArea.Width - SecondMountains.Width) / 2, SecondCastle.Location.Y + SecondCastle.Height + 12);
            SecondSea.Location = new Point(SecondArea.Location.X + (SecondArea.Width - SecondSea.Width) / 2, SecondMountains.Location.Y + SecondMountains.Height + 12);
            if ( countSea.Visible == false )
                btnOk.Location = new Point((Size.Width - btnOk.Width)/2, countCastle.Location.Y + countCastle.Height + 12);

            string s = (int)tick.TotalHours + " год. " + (int)tick.TotalMinutes + " хв. " + (int)tick.TotalSeconds + " с.";
            time.Text += " " + s;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
